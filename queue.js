let collection = [];

// Write the queue functions below.


function print() {
    //Print queue elements.
    return collection
}

function enqueue(element){
    collection.push(element);

    return collection
}

function dequeue(){
    collection.shift();
    return collection;
   
}

function front(){
    return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){
    if (collection.length) {
        return false
    }

    else{

        return true
    }
}

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
